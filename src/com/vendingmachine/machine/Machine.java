package com.vendingmachine.machine;


import com.vendingmachine.items.Item;
import com.vendingmachine.money.Coin;

import java.util.List;

public interface Machine {
    void initializeMachine();

    Item selectItem();

    void removeSelectedItem();

    void collectMoney(Object money) throws Exception;

    List<Coin> refund();

    void resetMachine();
}
