package com.vendingmachine.machine;

import com.vendingmachine.exception.CardNotValidException;
import com.vendingmachine.exception.ItemNotAvailableException;
import com.vendingmachine.exception.NoChangeLessTenCentException;
import com.vendingmachine.items.Item;
import com.vendingmachine.items.SnackSlots;
import com.vendingmachine.money.Card;
import com.vendingmachine.money.Coin;
import com.vendingmachine.money.Notes;
import com.vendingmachine.tools.Display;
import com.vendingmachine.tools.KeyPad;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SnacksMachine implements Machine {
    //num of SnackSlotColumn
    private final int col = 5;
    //num of SnackSlotRows
    private final int row = 5;
    //current amount on machine
    private double currentAmount;
    //var for snacks slot
    private SnackSlots snacks;
    //current item selected
    private Item selectedItem = null;

    public SnacksMachine() {
        initializeMachine();
    }

    /**
     * initialize vending machine with default snacks
     */
    @Override
    public void initializeMachine() {
        currentAmount = 0;
        snacks = new SnackSlots(row, col);
        //items file name
        String itemsCSVFileName = "items.csv";
        snacks.setItems(fillItemsSlots(itemsCSVFileName));
    }

    /**
     * @return current amount of money in machine
     */
    public double getCurrentAmount() {
        return currentAmount;
    }

    /**
     * @return snacks
     */
    public SnackSlots getSnacks() {
        return snacks;
    }

    /**
     * @return selected item in machine
     */
    public Item getSelectedItem() {
        return selectedItem;
    }

    /**
     * @param filename file name of snacks items
     * @return 2d array of items in machine
     */
    public Item[][] fillItemsSlots(String filename) {
        Item[][] items = snacks.getItems();
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(filename).getAbsolutePath()));
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                String[] item = line.split(",");
                items[i / row][i % col] = new Item(item[0], Double.parseDouble(item[1]), Integer.parseInt(item[2]), i / row, i % col);
                i++;
            }
        } catch (IOException e) {
            Display.printOnScreen(e.getStackTrace());
        }
        return items;
    }

    /**
     * select item to buy
     *
     * @return selected item
     */
    @Override
    public Item selectItem() {
        if (selectedItem == null) {
            int[] itemKey = KeyPad.useKeyPad("[0-4]", 2, "Item Key");
            if (itemKey == null) {
                selectItem();
                return null;
            }
            selectedItem = checkItem(itemKey[0], itemKey[1]);
        }
        return selectedItem;
    }

    /**
     * remove selected item
     */
    @Override
    public void removeSelectedItem() {
        selectedItem = null;
    }

    /**
     * @param row item row
     * @param col item col
     * @return item if available
     */
    public Item checkItem(int row, int col) {
        if (snacks.getItems()[row][col].getQuantity() > 0) {
            return snacks.getItems()[row][col];
        }
        throw new ItemNotAvailableException("Item not Available!!");
    }

    /**
     * @param money type of money[Coin,Card,Notes]
     * @throws Exception CardNotValidException, Not Accepted type of money
     */
    @Override
    public void collectMoney(Object money) throws Exception {
        if (money instanceof Coin) {
            currentAmount += ((Coin) money).getDenomination();
        } else if (money instanceof Notes) {
            currentAmount += ((Notes) money).getDenomination();
        } else if (money instanceof Card && (selectedItem.getPrice() - currentAmount) > 0) {
            if (((Card) money).useThisCard(selectedItem.getPrice() - currentAmount)) {
                currentAmount = selectedItem.getPrice();
            } else
                throw new CardNotValidException("Please Enter valid Card");
        }
        Display.printOnScreen("Amount =" + currentAmount);
    }

    /**
     * buy item and remove it and snacks
     *
     * @return item
     */
    public Item collectItem() {
        Item temp = selectedItem;
        selectedItem.setQuantity(selectedItem.getQuantity() - 1);
        currentAmount -= selectedItem.getPrice();
        System.out.println(currentAmount);
        snacks.dropSelectedItem(selectedItem);
        selectedItem = null;
        return temp;
    }

    /**
     * @return current amount of money is enough to buy selected item
     */
    public boolean isMoneyEnough() {
        return currentAmount >= selectItem().getPrice();
    }

    /**
     * @return return money as coin from largest to smallest Denomination
     */
    @Override
    public List<Coin> refund() {
        List<Coin> coins = new ArrayList<>();
        while (currentAmount > 0) {
            int i = 0;
            currentAmount = Math.round(currentAmount * 100) / 100.0;
            for (Coin coin : Coin.values()) {
                if (currentAmount >= coin.getDenomination()) {
                    currentAmount -= coin.getDenomination();
                    coins.add(coin);
                    break;
                } else if (currentAmount > 0 && i++ == Coin.values().length - 1) {
                    Display.printOnScreen(currentAmount);
                    throw new NoChangeLessTenCentException("No change less than a 10 cent");
                }
            }

        }
        return coins;
    }

    /**
     * reset all machine var
     */
    @Override
    public void resetMachine() {
        initializeMachine();
    }
}
