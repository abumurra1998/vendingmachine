package com.vendingmachine.machine;

import com.vendingmachine.money.Card;
import com.vendingmachine.money.Coin;
import com.vendingmachine.money.Notes;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws Exception {
        SnacksMachine machine = new SnacksMachine();
        Scanner scanner = new Scanner(System.in);
        int choice;
        while (true) {
            System.out.println(machine.getSnacks().toString());
            machine.selectItem();
            System.out.println("You Select :" + machine.getSelectedItem().toString() + "\n1-continue\n2-select other item");
            choice = scanner.nextInt();
            if (choice == 1) {
                while (!machine.isMoneyEnough()) {
                    System.out.println("Please select method to insert money\n1-Coin\n2-Notes\n3-Card");
                    choice = scanner.nextInt();
                    if (choice == 1) {
                        System.out.println("Insert Coins[C10,C20,C50,$1]");
                        String input = scanner.next();
                        try{
                            machine.collectMoney(Coin.valueOf(input));
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }else if (choice == 2) {
                        System.out.println("Insert Notes[$20,$50]");
                        String input = scanner.next();
                        try{
                            machine.collectMoney(Notes.valueOf(input));
                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                    }else if (choice == 3){
                        machine.collectMoney(new Card(""));
                        System.out.println(String.format("%.2f",machine.getCurrentAmount())+" USD has been debited");
                    }
                }
                System.out.println(machine.collectItem().toString());
                if(machine.getCurrentAmount()>0){
                    System.out.println("Do you want to refund\n1-Yes");
                    if(scanner.nextInt() != 1) {
                        continue;
                    }
                }
                System.out.println(machine.refund().toString());
                System.out.println("Good Bye!");
            } else if (choice == 2)
                machine.removeSelectedItem();
            choice = 0;
        }
    }
}
