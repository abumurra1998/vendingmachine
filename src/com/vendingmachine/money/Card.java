package com.vendingmachine.money;

public class Card {
    private String cardId;

    public Card(String cardId) {
        this.cardId = cardId;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }
    public boolean useThisCard(double value){
        // prossess to buy from this card
        return true;
    }
}
