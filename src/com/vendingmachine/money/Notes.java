package com.vendingmachine.money;

public enum Notes {
    $20(20.0), $50(50.0);
    private double denomination;

    Notes(double denomination) {
        this.denomination = denomination;
    }

    public double getDenomination() {
        return denomination;
    }
}
