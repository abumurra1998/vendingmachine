package com.vendingmachine.money;

public enum Coin {
    $1(1.0), C50(0.5), C20(0.2), C10(0.1);
    private final double denomination;

    Coin(double denomination) {
        this.denomination = denomination;
    }

    public double getDenomination() {
        return denomination;
    }

}
