package com.vendingmachine.tools;

import java.util.Scanner;
import java.util.regex.Pattern;

public class KeyPad {

    public static int[] useKeyPad (String regex,int num,String str){
        Display.printOnScreen("Please Enter " + str);
        Scanner scanner = new Scanner(System.in);
        String key = scanner.nextLine().trim().replaceAll(" ","");
        if(!validateKey(key,regex) || key.length() != num) {
            Display.printOnScreen("Please Enter valid "+ str);
            return null;
        }
        int [] temp = new int[key.length()];
        for(int i = 0; i < key.length();i++){
            temp[i] = Integer.parseInt(key.charAt(i)+"");
        }
        return temp;
    }

    static boolean validateKey(String key,String regex) {
        String reg = "";
        for(int i = 0; i < key.length();i++){
            reg +=regex;
        }
        return Pattern.matches(reg,key);
    }
}
