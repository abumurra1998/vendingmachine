package com.vendingmachine.items;

public class SnackSlots {
    //num of columns
    private int col;
    //num of rows
    private int row;
    //var of items
    private Item[][] items;

    public SnackSlots(int row,int col) {
        this.col = col;
        this.row = row;
        items = new Item[row][col];
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public Item[][] getItems() {
        return items;
    }

    public void setItems(Item[][] items) {
        this.items = items;
    }

    public void dropSelectedItem(Item item){
        items[item.getRow()][item.getCol()] = item;
    }
    @Override
    public String toString() {
        String temp = "Key : item , price";
        for (int i = 0; i < row * col; i++) {
            temp += "\n" + i / row + i % col + " : " + items[i / row][i % col];
        }
        return temp;
    }

}
