package com.vendingmachine.items;

public class Item {
    private String name;
    private double price;
    private int quantity;
    private int row;
    private int col;

    public Item(String name, double price,int quantity,int row,int col) {
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.row = row;
        this.col = col;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public String toString() {
//        return name + " , " +String.format("%.2f" ,price);
        return name + " , " +price;
    }
}
