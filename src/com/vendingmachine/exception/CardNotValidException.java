package com.vendingmachine.exception;

import com.vendingmachine.tools.Display;

public class CardNotValidException extends RuntimeException {
    private String message;

    public CardNotValidException(String string) {
        this.message = string;
        Display.printOnScreen(this.message);
    }
    @Override
    public String getMessage(){
        return message;
    }
}
