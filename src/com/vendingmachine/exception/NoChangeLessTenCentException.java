package com.vendingmachine.exception;

import com.vendingmachine.tools.Display;

public class NoChangeLessTenCentException extends RuntimeException {
    private String message;

    public NoChangeLessTenCentException(String string) {
        this.message = string;
        Display.printOnScreen(this.message);
    }
    @Override
    public String getMessage(){
        return message;
    }
}
