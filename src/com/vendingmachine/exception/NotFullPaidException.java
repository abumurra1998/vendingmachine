package com.vendingmachine.exception;

import com.vendingmachine.tools.Display;

public class NotFullPaidException extends RuntimeException {
    private String message;

    public NotFullPaidException(String string) {
        this.message = string;
        Display.printOnScreen(this.message);
    }
    @Override
    public String getMessage(){
        return message;
    }
}
